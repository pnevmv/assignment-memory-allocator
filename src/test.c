#define _DEFAULT_SOURCE
#include "test.h"
#define MIN_CAPACITY (24)

//Обычное успешное выделение памяти
static bool test1(void* heap) {
    bool result = false;
    printf("test #1 started\n");
    printf("Normal memory allocation success\n");

    //Просто выделяем
    size_t* array = _malloc(sizeof(size_t));

    struct block_header* block = block_get_header(array);
    if (array && !block->is_free && !block->capacity.bytes < MIN_CAPACITY) result = true;
    //Смотрим что все в норме
    debug_heap(stdout, heap);
    //Просто освобождаем
    _free(array);
    
    printf("test #1 finished\n");
    return result;
}

//Освобождение одного блока из нескольких выделенных
static bool test2(void* heap) {
    bool result = false;
    printf("test #2 started\n");
    printf("Freeing one block from several allocated\n");

    //выделяем память для трех переменных
    size_t* smt1 = _malloc(sizeof(size_t));
    uint64_t* smt2 = _malloc(sizeof(uint64_t));
    int64_t* smt3 = _malloc(sizeof(int64_t));
    
    debug_heap(stdout, heap);
    struct block_header* block1 = block_get_header(smt1);
    struct block_header* block2 = block_get_header(smt2);
    struct block_header* block3 = block_get_header(smt3);
    if (smt1 && smt2 && smt3 && 
    !block1->is_free && !block2->is_free && !block3->is_free &&
    !(block1->capacity.bytes < MIN_CAPACITY) && !(block2->capacity.bytes < MIN_CAPACITY) && !(block3->capacity.bytes < MIN_CAPACITY)) result = true;
    
    //освобождаем блок одной переменной
    _free(smt2);
    //проверяем, действительно ли память была освобождена
    debug_heap(stdout, heap);
    
    struct block_header* block = block_get_header(smt2);
    if (!block2->is_free || block1->is_free || block3->is_free) result = false;
    
    _free(smt1);
    _free(smt3);

    printf("\ntest #2 finished\n");
    return result;
}

//Освобождение двух блоков из нескольких выделенных.
static bool test3(void* heap) {
    bool result = false;
    printf("test #3 started\n");
    printf("Freeing two blocks from several allocated ones.\n");

    //также выделяем память для трех переменных
    int* v1 = _malloc(sizeof(size_t));
    uint32_t* v2 = _malloc(sizeof(uint64_t));
    int32_t* v3 = _malloc(sizeof(int64_t));
    struct block_header* block1 = block_get_header(v1);
    struct block_header* block2 = block_get_header(v2);
    struct block_header* block3 = block_get_header(v3);
    
    debug_heap(stdout, heap);
    if (v1 && v2 && v3 && 
    !block1->is_free && !block2->is_free && !block3->is_free &&
    !(block1->capacity.bytes < MIN_CAPACITY) && !(block2->capacity.bytes < MIN_CAPACITY) && !(block3->capacity.bytes < MIN_CAPACITY)) result = true;
    size_t block3_capacity = block3->next->capacity.bytes;
    //освобождаем два блока
    _free(v2);
    _free(v3);
    //проверяем, действительно ли память была освобождена
    debug_heap(stdout, heap);
    
    if (!block2->is_free || !block3->is_free || block3->capacity.bytes != block3_capacity + 41) result = false;
    
    _free(v1);

    printf("\ntest #3 finished\n");
    return result;
}

//Память закончилась, новый регион памяти расширяет старый.
static bool test4(void* heap) {
    bool result = false;
    printf("test #4 started\n");
    printf("The memory has run out, the new memory region expands the old one.\n");
    
    debug_heap(stdout, heap);
    //Выделяем память
    void* something1 = _malloc(7000);
    void* something2 = _malloc(4500);
    
    debug_heap(stdout, heap);
    
    //Проверка, действительно ли память занята
    struct block_header* block1 = block_get_header(something1);
	struct block_header* block2 = block_get_header(something2);
    if (!block1->is_free && !block2->is_free 
    && something1 && something2 
    && (uint8_t*) block1->contents + block2->capacity.bytes == (uint8_t*) block2) result = true;
    
    //Освобождаем все
    _free(something1);
    _free(something2);
    debug_heap(stdout, heap);

    printf("\ntest #4 finished\n");
    return result;
}

//Память закончилась, старый регион памяти не расширить из-за другого
//выделенного диапазона адресов, новый регион выделяется в другом месте.
static bool test5(void* heap) {
    bool result = false;
    printf("test #5 started\n");
    printf("The memory has run out,\nthe old memory region cannot be expanded due\nto a different allocated range of addresses,\nthe new region is allocated in a different place.\n");

    debug_heap(stdout, heap);
    void* var1 = _malloc(4096);
    struct block_header* block1 = heap;
    debug_heap(stdout, heap);

    while (block1->next != NULL) block1 = block1->next;
    map_pages((uint8_t *) block1 + size_from_capacity(block1->capacity).bytes, 1000, MAP_FIXED);
    void* var2 = _malloc(1024);
    debug_heap(stdout, heap);
    
    struct block_header* block2 = block_get_header(var2);
    if (var2 && block2 != block1) result = true;

    _free(var1);
    _free(var2);
    
    printf("\ntest #5 finished\n");
    return result;
}

void start_test() {
    printf("Tests started");
    size_t count = 0;
    void* heap = heap_init(2);
    debug_heap(stdout, heap);
    if (test1(heap)) count++;
    if (test2(heap)) count++;
    if (test3(heap)) count++;
    if (test4(heap)) count++;
    if (test5(heap)) count++;
    printf("Finish, %zu/5 - success", count);
}
